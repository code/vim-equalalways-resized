equalalways\_resized.vim
========================

This tiny plugin extends `'equalalways'` to rebalance windows when Vim itself
is resized.

License
-------

Copyright (c) [Tom Ryder][1].  Distributed under the same terms as Vim itself.
See `:help license`.

[1]: https://sanctum.geek.nz/
