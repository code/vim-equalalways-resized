"
" equalalways_resized: If 'equalalways' is set, extend it to VimResized
" events.
"
" Author: Tom Ryder <tom@sanctum.geek.nz>
" License: Same as Vim itself
"
if exists('loaded_equalalways_resized') || &compatible || v:version < 700
  finish
endif
let loaded_equalalways_resized = 1

" Add hook for VimResized event
augroup equalalways_resized
  autocmd!
  autocmd VimResized *
        \ if &equalalways | wincmd = | endif
augroup END
